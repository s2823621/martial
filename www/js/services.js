angular.module('starter.services', [])

.service('Firebase', function($q) {
  var $firebaseObj = new Firebase('https://brilliant-fire-2080.firebaseio.com/web/uauth');
  return {
      login: function($userObj)
      {
        var deferred = $q.defer();
        console.log($userObj);
        $firebaseObj.authWithPassword($userObj, function onAuth(err, user) {
            if (err) {
                deferred.reject(err);
            }

            if (user) {
                deferred.resolve(user);
            }
        });
        return deferred.promise;
      }
  }
});