$scope.createUser = function(authData) 
  {
    var acc = $scope.firebaseUserObj;
    var newUser = $scope.regData.email;
    console.log('Creating new user', newUser);
    acc.set(
    {
      newUser: 
        {
          clubName: $scope.regData.clubName,
          mainLocation: $scope.regData.mainLocation,
          address: $scope.regData.address,
          contactNumber: $scope.regData.contactNumber,
          contactName: $scope.regData.contactName,
          website: $scope.regData.website
        }
    },
      function(error, userData) 
        {
          console.log("Error: " + error.code);
        });
        if($scope.password == $scope.confirmPassword) 
        {  
        var ref = $scope.firebaseAuthObj;
        ref.createUser(
          {
            email: $scope.regData.email,
            password: $scope.regData.password,
          }, function(userData) 
              {
                if (error) 
                  {
                    switch (error.code) 
                    {
                      case "EMAIL_TAKEN": console.log("The new user account cannot be created because the email is already in use.");
                      break;
                      case "INVALID_EMAIL": console.log("The specified email is not a valid email.");
                      break;
                      default: console.log("Error creating user:", error);
                    }
                  } 
                  else 
                    {
                      console.log("Successfully created user account with uid:", userData.uid);
                      $window.location.href = '#/login';
                    }
              });
      } else {
        console.log("Passwords must match");
      }
  }     