// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'firebase', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'AppCtrl'
  })

  .state('register', {
      url: '/register',
      templateUrl: 'templates/register.html',
      controller: 'AppCtrl'
    
    })

  /*.state('account', {
      url: '/account',
      templateUrl: 'templates/account.html',
      controller: 'AppCtrl'
    
    }) */

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/sidemenu.html',
    controller: 'AppCtrl',
    //authRequired: true
  })

    .state('app.mainmenu', {
      url: '/mainmenu',
      views: {
        'menuContent': {
          templateUrl: 'templates/mainmenu.html',
          controller: 'MainMenuCtrl',
          //authRequired: true
        }
      }
    })
    
    .state('app.classlist', {
      url: '/classlist',
      views: {
        'menuContent': {
          templateUrl: 'templates/classlist.html',
          controller: 'ClassListCtrl',
          authRequired: true
        }
      }
    })

    .state('app.account', {
      url: '/account',
      views: {
        'menuContent': {
          templateUrl: 'templates/account.html',
          controller: 'AppCtrl',
          authRequired: true
        }
      }
    })
    
    .state('app.createclass', {
      url: '/createclass',
      views: {
        'menuContent': {
          templateUrl: 'templates/createclass.html',
          controller: 'CreateClassCtrl',
          authRequired: true
        }
      }
    })
    
    .state('app.studentlist', {
      url: '/studentlist',
      views: {
        'menuContent': {
          templateUrl: 'templates/studentlist.html',
          controller: 'StudentListCtrl',
          authRequired: true
        }
      }
    })
    
    .state('app.createstudent', {
      url: '/createstudent',
      views: {
        'menuContent': {
          templateUrl: 'templates/createstudent.html',
          controller: 'CreateStudentCtrl',
          authRequired: true
        }
      }
    })
    
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
