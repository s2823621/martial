angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $q, $rootScope, $window, $firebaseObject) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};
  $scope.regData = {};

  $scope.firebaseAuthObj = new Firebase('https://brilliant-fire-2080.firebaseio.com/web/uauth');
  $scope.firebaseUserObj = new Firebase('https://brilliant-fire-2080.firebaseio.com/users');
  $scope.fb = new Firebase ('https://brilliant-fire-2080.firebaseio.com');

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.firebase_auth = function(userObj) {
    // Handle Email/Password login
    // returns a promise
    var deferred = $q.defer();
    console.log(userObj);
    $scope.firebaseAuthObj.authWithPassword(userObj, function onAuth(err, user) {
        if (err) {
            deferred.reject(err);
        }

        if (user) {
            deferred.resolve(user);
        }
    });
    return deferred.promise;
  }

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    $scope.firebase_auth($scope.loginData).then(function(user) {
        console.log("Login Successful");
        $window.location.href = '#/app/mainmenu';
    }, function(err) {
        alert(err + " Login Unsuccessful");
    })
  };

  $scope.goRegister = function() {
    $window.location.href = '#/register';
  };

  $scope.goAccount = function() {
    $window.location.href = '#/account';
  };

  
  $scope.logOut = function() {
    $scope.firebaseAuthObj.unauth();
    console.log("You are now logged out");
    $window.location.href = '#/login';
  };

  $scope.list = function() {
    var fbAuth = $scope.fb.getAuth();
    if(fbAuth) {
      var sync = $firebaseObject($scope.fb.child("users/" + fbAuth.uid));
      sync.$bindTo($scope, "regData");
    }
  }

  $scope.saveInfo = function() {
    if($scope.regData !== "") {
      if($scope.regData.hasOwnProperty("regData") !== true) {
        $scope.regData.information = [];
      }
      $scope.regData.push(x);
      console.log("successful push");
    } else {
      console.log(error);
    }
  }

/*
  $scope.closeRegister = function() {
    $scope.modal.hide();
  };


  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };
*/

  $scope.authDataCallback = function(authData) {
    if(authData) {
      console.log("User " + authData.uid + " is logged in with " + authData.provider);
    } else {
      console.log("User is logged out");
    }
  }

$scope.createUser = function(authData) 
  {
        var ref = $scope.firebaseAuthObj;
        ref.createUser(
          {
            email: $scope.regData.email,
            password: $scope.regData.password,
          }, function(userData, error) 
              {
                if (error) 
                  {
                    switch (error.code) 
                    {
                      case "INVALID_USER": alert("The specified user account does not exist.");
                      break;
                      case "INVALID_PASSWORD": alert("The specified user account password is incorrect.");
                      break;
                      case "EMAIL_TAKEN": alert("The new user account cannot be created because the email is already in use.");
                      break;
                      case "INVALID_EMAIL": alert("The specified email is not a valid email.");
                      break;
                      default: alert("Error creating user:", error.code);
                    }
                  } 
                  else 
                    {
                      alert("Successfully created user account");
                      $window.location.href = '#/login';
                    }
              });
      }
})

.controller('AccountCtrl', function() {})

.controller('MainMenuCtrl', function() {})

.controller('SideMenuCtrl', function() {})

.controller('LogInCtrl', function() {})

.controller('RegisterCtrl', function() {})

.controller('ClassListCtrl', function() {})

.controller('CreateClassCtrl', function() {})

.controller('StudentListCtrl', function() {})

.controller('CreateStudentCtrl', function() {})

